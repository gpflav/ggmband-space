import React, {Component} from 'react';
import star from './star.png';
import { StickyContainer, Sticky } from 'react-sticky';

class Setlist extends Component {
    state = {
        displayContents: false
    }
    
    click2display = () => {
        this.setState({
            songs: [
                { id: 1, title: "#1. Feeling Good", content: '공연의 시작을 알리는 노래\n오늘 하루, 여러분들이\n좋은 기분 듬뿍 받아갔으면', img_src: './src/img/Image3.png'},
                { id: 2, title: '#2. 2002' , content:'당신에게 2002년은\n어떤 순간이었나요?\n그 때로 돌아가볼까요?', img_src: './img/Image3.png'},
                { id: 3, title: '#3. 오디션' , content:'Time 2 Rock\nReady, Go!\n떨리는 지금', img_src: './img/Image3.png'},
             ], //<img src={obj.img_src} alt={obj.title} height="100" width="100"></img>
            displayContents: !this.state.displayContents
        })
    }


    render() {
        let contents = null;

        if ( this.state.displayContents ) {
            const EnterText = (text) => {
                return text.split('\n').map((line, index) => <p className="small-eng-font song-content">{line}</p>)
              }
            contents = (
            <div className="ani">
                <br></br>
                <p className="big-font">오늘 들려드릴 노래는<br></br>아래와 같습니다</p>
                <img className="star" src={star} alt="star" height="100" width="60"></img>
                <p className="big-font">&lt;1부&gt;</p>
                <br></br>
                { this.state.songs.map((obj, index) => {
                    return <div key={obj.id}>
                        
                        <p className="small-eng-font">{obj.title}</p>
                        <div className="small-font">
                            {EnterText(obj.content)}
                        </div> 
                        <br></br>
                    </div>

                    
                    })}
                <br></br>
                <br></br>
            </div>
            )
        }
        return (
            <div>  
                <StickyContainer>  
                
                <Sticky>{({ style }) => 
                <div className="sticky" style={style}>
                    <h2 className = 'sticky underline_text pointer' onClick={this.click2display}>셋리스트</h2>
                    <hr width="85" className="underline"></hr>
                </div>
                }
                </Sticky>
                
                {contents}    
                </StickyContainer>       
            </div>
            );
    }

}

export default Setlist;