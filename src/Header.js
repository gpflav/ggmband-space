import React from 'react';
import Animista, { AnimistaTypes } from "react-animista";

const Header = () => {

  
    return (
     <div >
            <Animista type={AnimistaTypes.SCALE_UP_TOP}>Basic animation</Animista>
                <h1 className = 'f1 bounce-in-top' class="animated fadeInUp ease-out-circ d2 a-1 f2 fw3" >구교민밴드</h1>
                <p className = 'f4'>#ggmband #홍대우주정거장 #2019.12.26</p>
                <h2 className = 'f1' >Programs</h2>
     </div>
    )
   };
   
   export default Header;
