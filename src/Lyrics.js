import React from 'react';

const Lyrics = ( props ) => {

  const EnterText = (text) => {
    return text.split('\n').map((line, index) => <p key = {index}>{line}</p>)
  }

  return (
   <div className="Lyrics f5 lyrics">
    {EnterText(props.title)}
   </div>
  )
 };
 
 export default Lyrics;
