import React, {Component} from 'react';
import star from './star.png';
import { StickyContainer, Sticky } from 'react-sticky';

class BandIntro extends Component {
    state = {
        displayContents: false
    }
    
    click2display = () => {
        this.setState({
            displayContents: !this.state.displayContents
        })
    }
    render() {
        let contents = null;

        if ( this.state.displayContents ) {
            contents = (
            <div className="ani">
                <p className="big-font"> 안녕하세요<br></br>구교민밴드입니다</p>
                <img className="star-content" src={star} alt="star" height="100" width="60"></img>
                <p className="small-font"> 우리는 어느 날 문득<br></br>교민이에게 이런 카톡을 받았습니다</p>
                <br></br>
                <p className="big-font">'같이 밴드할래?'</p>
                <br></br>

                <p className="small-font"> 2019년 6월, 그렇게 우리는<br></br>'구교민밴드'를 결성하게 됩니다<br></br><br></br>음악도 물론 좋지만<br></br>사람이 좋아서<br></br>6개월을 함께 했습니다<br></br><br></br>따뜻한 연말,<br></br>지난 6개월의 추억을<br></br>여러분과 나누고자 합니다<br></br></p>
                <p className="big-font">여기, 우주정거장에서</p>
                <br></br>
                <br></br>
            </div>
            )
        }
        return (
            <div>  
                <StickyContainer>  
                
                <Sticky>{({ style }) => 
                <div className="sticky" style={style}>
                    <h2 className = 'sticky underline_text pointer' onClick={this.click2display}>밴드 소개</h2>
                    <hr width="85" className="underline"></hr>
                </div>
                }
                </Sticky>
                
                {contents}    
                </StickyContainer>       
            </div>
            );
    }

}

export default BandIntro;