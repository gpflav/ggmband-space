import React from 'react';
import ReactDOM from 'react-dom';
import './index.css'; // in the same directory 
//import Hello from './Hello'; // ./App.js
//import * as serviceWorker from './serviceWorker';
import 'tachyons';
import App from './App';

// important : render whatever 
//ReactDOM.render(<Hello greeting={"Good Bye"}/>, document.getElementById('root'));

ReactDOM.render(<App />, document.getElementById('root'));