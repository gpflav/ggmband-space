import React, {Component} from 'react';
import star from './star.png';
import { StickyContainer, Sticky } from 'react-sticky';

class ProgramIntro extends Component {
    state = {
        displayContents: false
    }
    
    click2display = () => {
        this.setState({
            displayContents: !this.state.displayContents
        })
    }
    render() {
        let contents = null;

        if ( this.state.displayContents ) {
            contents = (
            <div className="ani">
                <br></br>
                <p className="big-font">오늘 공연은<br></br>이렇게 진행됩니다</p>
                <img className="star-content" src={star} alt="star" height="100" width="60"></img>
                <p className="small-font">&lt;공연 일시&gt;<br></br>2019년 12월 26일 목요일 8PM</p>
                <br></br>
                <p className="small-font">&lt;공연 장소&gt;<br></br>홍대 우주정거장 (와우산로 27길 16)</p>
                <br></br>
                <p className="small-font">&lt;1부&gt;<br></br>20:00-21:00<br></br><br></br>&lt;인터미션&gt;<br></br>21:00-21:15<br></br><br></br>&lt;2부&gt;<br></br>21:15-22:00<br></br><br></br>외부 음식 반입 가능합니다<br></br>공연 종료 후 새벽 2시까지<br></br>자유롭게 놀다가실 수 있습니다</p>
                <br></br>
                <br></br>
            </div>
            )
        }
        return (
            <div>  
                <StickyContainer>  
                
                <Sticky>{({ style }) => 
                <div className="sticky" style={style}>
                    <h2 className = 'sticky underline_text pointer' onClick={this.click2display}>공연 소개</h2>
                    <hr width="85" className="underline"></hr>
                </div>
                }
                </Sticky>
                
                {contents}    
                </StickyContainer>       
            </div>
            );
    }

}

export default ProgramIntro;