import React from 'react';
import './App.css';
//import Header from './Header';
import Parts from './Parts';
//import { StickyContainer, Sticky } from 'react-sticky';
//import Animista, { AnimistaTypes } from "react-animista";
import star from "./star.png";
import BandIntro from "./BandIntro.js";
import ProgramIntro from "./ProgramIntro.js";
import Ask from "./Ask.js";
import Setlist from "./Setlist.js";



//<img src={logo} className="App-logo" alt="logo" />
// className="App-header"
// className="App"
// <h1 className = 'f1' class="animated fadeInUp ease-out-circ d2 a-1 f2 fw3" >구교민밴드</h1>
// <Animista type={AnimistaTypes.SCALE_UP_TOP}></Animista>
function App() {
  return (
    <div >
            <p className="first-title">구교민밴드 첫 번째 단독 공연</p>
            <h3 className="second-title">우리는 우주정거장에서 만나자</h3>
            <img className="star" src={star} alt="star" height="100" width="60"></img>
          <section className="section">
            <BandIntro />
          </section>
          <section className="section">
            <ProgramIntro />
          </section>
          <section className="section">
            <Setlist />
          </section>
          <section className="section">
            <Ask />
          </section>        
      <footer>
      </footer>
    </div>
  );
}

export default App;
