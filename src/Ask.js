import React, {Component} from 'react';
import star from './star.png';
import { StickyContainer, Sticky } from 'react-sticky';

class Ask extends Component {
    state = {
        displayContents: false
    }
    
    click2display = () => {
        this.setState({
            displayContents: !this.state.displayContents
        })
    }
    render() {
        let contents = null;

        if ( this.state.displayContents ) {
            contents = (
            <div className="ani">
                <br></br>
                <p className="big-font">궁금한 점은<br></br>편하게 물어보세요</p>
                <img className="star" src={star} alt="star" height="100" width="60"></img>
                <p className="small-eng-font">&lt;Instagram&gt;<br></br>@kgmband</p>
                <br></br>
                <p className="big-font">&lt;리더 구교민&gt;<br></br>010-3794-3407<br></br>fp51tzo@naver.com</p>
                <br></br>
                <br></br>
            </div>
            )
        }
        return (
            <div>  
                <StickyContainer>  
                
                <Sticky>{({ style }) => 
                <div className="sticky" style={style}>
                    <h2 className = 'sticky underline_text pointer' onClick={this.click2display}>문의하기</h2>
                    <hr width="85" className="underline"></hr>
                </div>
                }
                </Sticky>
                
                {contents}    
                </StickyContainer>       
            </div>
            );
    }

}

export default Ask;